# vue-3 ts

## Project setup
```
npm install
```
Set all necessary variables in a .env.local file
For more info check [Modes and Environment Variables](https://cli.vuejs.org/guide/mode-and-env.html)

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


const express = require('express')
const fs = require('fs')
const app = express()
const cors = require('cors')
const uuid = require('uuid')

const port = process.env.PORT || 3000

const server = require('http').createServer({
  key: fs.readFileSync(__dirname + '/selfsigned.key'),
  cert: fs.readFileSync(__dirname + '/selfsigned.crt')
}, app)

const io = require('socket.io')(server, { origins: '*:*' })

app.use(cors())
app.options('*', cors()) // include before other routes

const playersLobby = {};
const playersReady = {};
const games = {};
let lobbyGame = '';

const roles = [
  'werewolve',
  'werewolve',
  'seer',
  // 'doctor',
  // 'witch',
  // 'sherif',
];
const shuffle = function (arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
}

const onToggleTarget = {
  seer: (gameId, userId, targetId) => {
    if (!gameId) return console.log('no gameId');
    if (!games[gameId]) return console.log('no games[gameId]');
    const game = games[gameId];
    if (game.turn !== 'night') return console.log('cant peep in daytime');
    if (game.players[userId] !== 'seer') return console.log('wrong seer');
    const state = game.seerState || {};
    if (state.value) return console.log('already peeped');
    const found = game.village.find((row) => row.id == targetId);
    const target = found?.name || targetId;
    const value = game.players[targetId];
    game.seerState = { target, value };
  },

  werewolve: (gameId, userId) => {
    if (!gameId) return console.log('no gameId');
    if (!games[gameId]) return console.log('no games[gameId]');
    const game = games[gameId];
    if (game.turn !== 'night') return console.log('cant eat in daytime');
    if (game.players[userId] !== 'werewolve') return console.log('wrong werewolve');
    const state = {
      werewolves: {},
      targets: {},
      counts: {},
    };
    game.village.forEach((row) => {
      const role = game.players[row.id];
      if (role != 'werewolve') return;
      if (row.rip) return;
      state.werewolves[row.id] = row.name;
      const targetId = game.werewolve[row.id];
      if (!targetId) return;
      state.counts[targetId] = state.counts[targetId] || 0;
      state.counts[targetId]++;
      target = game.village.find((row) => row.id == targetId) || { name: targetId };
      state.targets[row.id] = target.name;
    })
    game.werewolveState = state;
  }
}

const prepareGame = function (playersArray) {
  const id = uuid.v4();
  const unasignedRoles = Object.assign([], roles);
  const game = {
    id,
    turn: 'night',
    players: {}, //id's of users
    started: {},
    village: [], //general info
  };
  shuffle(playersArray);
  shuffle(unasignedRoles);
  if (lobbyGame && games[lobbyGame]) delete games[lobbyGame];
  lobbyGame = id;
  playersArray.forEach(userId => {
    game.players[userId] = unasignedRoles.length ? unasignedRoles.pop() : 'villager';
  });
  games[id] = game;
  return id;
}

const abortGame = function (id) {
  if (!id || !games[id]) return;
  delete games[id];
}

const getRoleState = function (game, role) {
  const result = {};
  result.role = role;
  result.targets_day = game.villager || {} //add what the villager's are targeting
  result.targets_night = game[role] || {}; //add what the seer/werewolfs are targeting
  result.state = game[`${role}State`] || {};
  return result;
}

const continueUserGame = function (id, user) {
  if (!id) return console.log('no id 2');
  if (!games[id]) return console.log('no games[id]');
  const game = games[id];
  const role = game.players[user];
  const userInfo = game.village.find((villager) => villager.id == user);
  const player = Object.assign({}, userInfo, getRoleState(game, role));
  return {
    player,
    village: game.village,
    turn: game.turn
  };
}

const startUserGame = function (id, user) {
  if (!id) return console.log('no id 1');
  if (!games[id]) return console.log('no games[id]');
  if (!playersLobby[user]) return continueUserGame(id, user)
  const game = games[id];
  const village = [];
  let player = {}
  Object.keys(game.players).forEach(userId => {
    const role = game.players[userId];
    const villager = Object.assign({ rip: false }, playersLobby[userId])
    village.push(villager);
    if (user == userId) {
      Object.assign(player, villager, getRoleState(game, role));
    }
  });

  game.started[user] = true;
  game.village = village;
  delete playersReady[user];
  return { player, village, turn: game.turn }
}

io.on('connection', socket => {
  console.log('connection', socket.id);

  const joinGame = function (data, info) {
    const game = games[data.game];
    const players = Object.keys(game.players).length;
    const started = Object.keys(game.started).length;

    socket.leave('lobby');
    socket.join(game.id);
    const role = info.player.role;
    socket.join(`${game.id}/${role}`); //join villagers/warewolfs/... room
    socket.emit('gameStarted', game[role]); //emit all targets
    socket.emit('playersActive', info);
    if (players != started) return;
    Object.keys(game.players).forEach(userId => {
      delete playersLobby[userId];
    });
    const state = Object.assign(data, {
      turn: game.turn,
      state: game.state
    })
    io.to(game.id).emit('gameStarted', state);
  }

  socket.on('welcome', (user) => {
    if (user.gameId) {
      const data = { game: user.gameId, user: user.id };
      const info = startUserGame(data.game, data.user);
      if (!info) return socket.emit('gameInvalid', data);
      joinGame(data, info);
      return;
    }
    playersLobby[user.id] = user;
    socket.join('lobby');
  });

  socket.on('playerReady', (data) => {
    const player = playersLobby[data.user];
    if (!player) return;
    player.ready = data.ready;
    io.to('lobby').emit('playersLobby', playersLobby);
    if (data.ready) playersReady[player.id] = true;
    else delete playersReady[player.id];
    if (lobbyGame) {
      io.to('lobby').emit('gameAbort', abortGame(lobbyGame));
    }
    const playersArr = Object.keys(playersReady);
    if (playersArr.length >= 4) {
      io.to('lobby').emit('gameReady', prepareGame(playersArr));
    }
  });

  socket.on('listLobbyPlayers', () => {
    io.to('lobby').emit('playersLobby', playersLobby);
  });

  socket.on('startUserGame', (data) => {
    const info = startUserGame(data.game, data.user);
    if (!info) return socket.emit('gameInvalid', data);
    joinGame(data, info);
  });

  socket.on('toggleTarget', (data) => {
    const info = continueUserGame(data.game, data.user);
    if (!info) return socket.emit('gameInvalid', data);
    const targets = info.player[`targets_${info.turn}`];
    if (!data.target) delete targets[data.user];
    else targets[data.user] = data.target;
    const role = info.player.role;
    if (info.turn == 'day') games[data.game].villager = targets;
    else games[data.game][role] = targets;
    if (onToggleTarget[role]) onToggleTarget[role](data.game, data.user, data.target);
    const result = getRoleState(games[data.game], role);
    io.to(`${data.game}/${role}`).emit('toggleTarget', result);
  });

  socket.on('disconnect', () => {
    io.to('lobby').emit('playersLobby', playersLobby);
  });
})

server.listen(port, () => {
  console.log(`Server running on port: ${port}`)
})

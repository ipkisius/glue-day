import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { Components, Services } from "./primevue";
import primevue from "primevue/config";
import "@fortawesome/fontawesome-free/css/all.min.css";
import { io } from 'socket.io-client';

const app = createApp(App);
app.use(router);

app.provide('socketIOClient', io('http://localhost:3000', {
  transports: ['websocket']
}));

app.use(primevue, { ripple: true });

Components.forEach((comp) => {
  app.component(comp.name, comp.component)
})

Services.forEach((service) => {
  app.use(service);
})


app.mount("#app");


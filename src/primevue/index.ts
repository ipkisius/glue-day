import 'primeflex/primeflex.css';

// import 'primevue/resources/themes/bootstrap4-light-blue/theme.css';
// import 'primevue/resources/themes/bootstrap4-light-purple/theme.css';
// import 'primevue/resources/themes/bootstrap4-dark-blue/theme.css';
// import 'primevue/resources/themes/bootstrap4-dark-purple/theme.css';
// import 'primevue/resources/themes/md-light-indigo/theme.css';
// import 'primevue/resources/themes/md-light-deeppurple/theme.css';
// import 'primevue/resources/themes/md-dark-indigo/theme.css';
// import 'primevue/resources/themes/md-dark-deeppurple/theme.css';
// import 'primevue/resources/themes/mdc-light-indigo/theme.css';
// import 'primevue/resources/themes/mdc-light-deeppurple/theme.css';
// import 'primevue/resources/themes/mdc-dark-indigo/theme.css';
// import 'primevue/resources/themes/mdc-dark-deeppurple/theme.css';
import 'primevue/resources/themes/fluent-light/theme.css';
// import 'primevue/resources/themes/saga-blue/theme.css';
// import 'primevue/resources/themes/saga-green/theme.css';
// import 'primevue/resources/themes/saga-orange/theme.css';
// import 'primevue/resources/themes/saga-purple/theme.css';
// import 'primevue/resources/themes/vela-blue/theme.css';
// import 'primevue/resources/themes/vela-green/theme.css';
// import 'primevue/resources/themes/vela-orange/theme.css';
// import 'primevue/resources/themes/vela-purple/theme.css';
// import 'primevue/resources/themes/arya-blue/theme.css';
// import 'primevue/resources/themes/arya-green/theme.css';
// import 'primevue/resources/themes/arya-orange/theme.css';
// import 'primevue/resources/themes/arya-purple/theme.css';
// import 'primevue/resources/themes/nova/theme.css';
// import 'primevue/resources/themes/nova-alt/theme.css';
// import 'primevue/resources/themes/nova-accent/theme.css';
// import 'primevue/resources/themes/nova-vue/theme.css';
// import 'primevue/resources/themes/luna-amber/theme.css';
// import 'primevue/resources/themes/luna-blue/theme.css';
// import 'primevue/resources/themes/luna-green/theme.css';
// import 'primevue/resources/themes/luna-pink/theme.css';
// import 'primevue/resources/themes/rhea/theme.css';

import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';

import Toolbar from 'primevue/toolbar';
import Menu from 'primevue/menu';
import Menubar from 'primevue/menubar';
import Dialog from 'primevue/dialog';
import ConfirmPopup from 'primevue/confirmpopup';
import Toast from 'primevue/toast';
import Button from 'primevue/button';
import Card from 'primevue/card';
import InputText from 'primevue/inputtext';
import Dropdown from 'primevue/dropdown';

import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';

export const Components = [
  { name: 'p-toolbar', component: Toolbar },
  { name: 'p-menu', component: Menu },
  { name: 'p-menubar', component: Menubar },
  { name: 'p-dialog', component: Dialog },
  { name: 'p-confirm-popup', component: ConfirmPopup },
  { name: 'p-toast', component: Toast },
  { name: 'p-button', component: Button },
  { name: 'p-card', component: Card },

  { name: 'p-input-text', component: InputText },
  { name: 'p-dropdown', component: Dropdown },
];

export const Services = [
  ToastService,
  ConfirmationService,
]

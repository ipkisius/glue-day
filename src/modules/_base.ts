import { v4 as uuid } from 'uuid';

export function env(key: string, fallback?: string): string {
  if (process.env[key] !== undefined) return process.env[key];
  const appKey = `VUE_APP_${key}`;
  if (process.env[appKey] !== undefined) return process.env[appKey];
  return fallback || '';
}

export class Model {
  [key: string]: any;

  _createdAt = new Date();
  _updatedAt?: Date;

  _fillable: string[] = [];

  update(partial: any) {
    if (!partial) partial = {};

    this._fillable.forEach(key => {
      if (partial[key] === undefined) return;
      this[key] = partial[key];
    })

    return this;
  }

  generateUUID(): string {
    return uuid();
  }

  setDate(input: string | Date): Date {
    if (input instanceof Date) return input;
    return new Date(input);
  }

  set createdAt(value: string | Date) {
    this._createdAt = this.setDate(value);
  }

  get createdAt() {
    return this._createdAt;
  }

  set updatedAt(value: undefined | string | Date) {
    this._updatedAt = value === undefined ? undefined : this.setDate(value);
  }

  get updatedAt() {
    return this._updatedAt;
  }

  toJson(): any {
    const result: {
      [key: string]: any;
    } = {};
    this._fillable?.forEach(key => {
      result[key] = this[key];
    });
    return result;
  }

  toString(): string {
    return JSON.stringify(this.toJson());
  }
}

export class Service {

}

export class SharedService extends Service {
  #shared: { [key: string]: any } = {};

  provide(id: string, item: any) {
    this.#shared[id] = item;
  }

  inject(id: string) {
    return this.#shared[id];
  }
}

export const $shared = new SharedService();
import { ref } from 'vue';
import { Model, Service } from './_base';

export class User extends Model {
  id = '';
  name = '';
  image = '';
  gameId = '';

  _fillable = [
    'id',
    'name',
    'image',
    'gameId',
    'createdAt',
    'updatedAt',
  ];

  constructor(partial?: any) {
    super();
    if (!partial) partial = {};
    if (!partial.id) partial.id = this.generateUUID();
    this.update(partial);
  }
}

export class AuthService extends Service {
  #currentUser = ref(new User());

  constructor() {
    super();
    const currentUser = sessionStorage.getItem('currentUser');
    if (currentUser) {
      this.#currentUser.value.update(JSON.parse(currentUser));
    }
  }

  get currentUser() {
    return this.#currentUser;
  }

  async save(partial: any): Promise<User> {
    return new Promise((resolve, reject) => {
      try {
        const user = this.#currentUser.value.update(partial);
        sessionStorage.setItem('currentUser', user.toString());
        resolve(user);
      }
      catch (err) {
        reject(err);
      }
    })
  }

  async signIn(user: User): Promise<User> {
    return new Promise((resolve, reject) => {
      try {
        this.#currentUser.value = user;
        sessionStorage.setItem('currentUser', user.toString());
        resolve(user);
      }
      catch (err) {
        reject(err);
      }
    })
  }

  signOut() {
    sessionStorage.removeItem('currentUser');
    this.#currentUser.value = new User();
  }
}

export const $auth = new AuthService();

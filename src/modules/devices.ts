import { Model, Service } from './_base';

export class Device extends Model {
  id = '';
  status = '';
  events = 0;
  _conectedAt?: Date;

  _fillable = [
    'id',
    'status',
    'events',
    'conectedAt',
    'createdAt',
    'updatedAt',
  ];

  constructor(partial?: any) {
    super();
    if (!partial) partial = {};
    if (!partial.id) partial.id = this.generateUUID();
    this.update(partial);
  }

  set conectedAt(value: undefined | string | Date) {
    this._conectedAt = value === undefined ? undefined : this.setDate(value);
  }

  get conectedAt() {
    return this._conectedAt;
  }
}

export class DevicesService extends Service {
  #list: Device[] = [];

  async add(item: Device): Promise<Device> {
    return new Promise((resolve) => {
      item.updatedAt = new Date();
      this.#list.push(item);
      resolve(item);
    })
  }

  async get(id: number): Promise<Device> {
    return new Promise((resolve, reject) => {
      const found = this.#list[id];
      if (found) {
        resolve(found);
      }
      else {
        reject({ status: 404, message: 'Not Found' });
      }
    })
  }

  async list(): Promise<Device[]> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(this.#list), 1500);
    });
  }
}

export const $devices = new DevicesService();

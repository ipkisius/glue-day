import { Model, Service } from './_base';

// villager //man farmer
// werewolve //wolf
// doctor //man health worker
// seer //person getting massage
// witch //woman mage
// sherif //police officer

//detective

export class Role extends Model {
  name = '';
  image = '';
  icon = '';
  description: {
    tag: string;
    text: string;
  }[] = [];
  comments: string[] = [];
  target_day = (): string => '<i class="fas fa-dumpster-fire"/>';
  actions_day: any[] = [
    {
      label: (): string => 'Send to a better world',
      icon: (): string => 'fas fa-cloud-upload-alt',
      disabled: () => false,
      class: (): string => 'p-button-danger p-button-outlined',
      command: () => 'toggleTarget',
    }
  ];
  ready_day = (): boolean => true;

  target_night = (): string => '';
  actions_night: any[] = [];
  ready_night = (): boolean => true;

  _fillable = [
    'name',
    'image',
    'description'
  ];

  constructor(partial?: any) {
    super();
    if (!partial) partial = {};
    if (!partial.id) partial.id = this.generateUUID();
    this.update(partial);
  }

  getComments(): string[] {
    return this.comments;
  }

  getStateTemplate(state?: any): string {
    if (!state) return '';
    return '';
  }
}

class VillagerRole extends Role {
  name = 'Villager';
  image = '/assets/villager.jpg';
  icon = 'fas fa-drumstick-bite';
  description = [
    { tag: 'h', text: `Night Time` },
    { tag: 'p', text: `Have a nice nap, it may be your last` },
    { tag: 'h', text: `Day Time` },
    { tag: 'p', text: `Sharpen your pitchfork and murder some friends` },
  ];
  comments = [];

  ready_day = (): boolean => false;
}

class WerewolveRole extends Role {
  name = 'Werewolve';
  image = '/assets/werewolve.jpg';
  icon = 'fas fa-paw';
  description = [
    { tag: 'h', text: `Night Time` },
    { tag: 'p', text: `Time to feed on some friends` },
    { tag: 'h', text: `Day Time` },
    { tag: 'p', text: `Be nice and push someone else under the bus` },
  ];
  comments = [];

  target_night = () => '<i class="fas fa-drumstick-bite" />';
  actions_night = [
    {
      label: (): string => 'Eat',
      icon: (): string => 'fas fa-utensils',
      disabled: (viewer: Player, target: Player) => {
        if (!viewer.state) return false;
        if (!viewer.state.targets) return false;
        if (viewer.state.targets[viewer.id] == target.name) return true; //already selecterd this one
        return false;
      },
      class: (): string => 'p-button-danger p-button-outlined',
      command: () => 'toggleTarget',
    }
  ]
  ready_night = (state?: any): boolean => {
    if (!state.werewolves) return false;
    if (!state.counts) return false;
    const targets = Object.keys(state.counts);
    if (targets.length != 1) return false;
    return state.counts[targets[0]] == Object.keys(state.werewolves).length;
  };

  getStateTemplate(state: any): string {
    if (!state.werewolves) return '';
    const result: string[] = [];
    Object.keys(state.werewolves).forEach((uuid) => {
      const target = state.targets[uuid];
      const wolve = state.werewolves[uuid];
      if (target) {
        result.push(`<strong>${wolve}</strong> wants to eat <strong>${target}</strong>`);
      }
      else {
        result.push(`<strong>${wolve}</strong> is still browsing the menu`);
      }
    });
    if (!result.length) return '';
    return '<ul class="p-m-0"><li>' + result.join("</li>\r\n<li>") + '</li></ul>';
  }
}

class SeerRole extends Role {
  name = 'Seer';
  image = '/assets/seer.jpg';
  icon = 'fas fa-low-vision';
  description = [
    { tag: 'h', text: `Night Time` },
    { tag: 'p', text: `Time to peep in some holes` },
    { tag: 'h', text: `Day Time` },
    { tag: 'p', text: `Go out and spread the truth` },
  ];
  comments = [];

  target_night = (state?: any) => state.value;
  actions_night = [
    {
      label: (): string => 'Peep',
      icon: (): string => 'fas fa-eye',
      class: (): string => 'p-button-danger p-button-outlined',
      disabled: (viewer: Player, target: Player) => {
        if (viewer.id == target.id) return true;
        if (viewer.state.value) return true;
        return false;
      },
      command: () => 'toggleTarget',
    }
  ]

  getStateTemplate(state: any): string {
    if (!state.value) return 'You can have <strong>ONE</strong> reavaling dream about someone!';
    return `peeped in <strong>${state.target}'s</strong> room and found out that they are a <strong>${state.value}</strong>`;
  }
}

export class Player extends Model {
  static roleConstructors: { [key: string]: any } = {
    villager: VillagerRole,
    werewolve: WerewolveRole,
    seer: SeerRole,
  }

  id = '';
  name = '';
  image = '';
  ready = false;
  rip = false;
  targets_day: { [key: string]: string } = {};
  targets_night: { [key: string]: string } = {};
  state: { [key: string]: any } = {};
  _role = '';
  _roleInfo?: any;
  _fillable = [
    'id',
    'name',
    'image',
    'role',
    'ready',
    'targets_day',
    'targets_night',
    'state',
    'createdAt',
    'updatedAt',
  ];

  constructor(partial?: any) {
    super();
    if (!partial) partial = {};
    if (!partial.id) partial.id = this.generateUUID();
    this.update(partial);
  }

  set role(value: string) {
    this._role = value;
    this._roleInfo = new Player.roleConstructors[value]();
  }

  get role() {
    return this._role;
  }

  get icon() {
    return this._roleInfo?.icon;
  }

  get roleInfo() {
    return this._roleInfo;
  }

  get roleState() {
    if (!this._roleInfo) return '';
    return this._roleInfo.getStateTemplate(this.state)
  }

  getActions(turn: 'day' | 'night') {
    return this._roleInfo[`actions_${turn}`];
  }

  getComments(player: Player): string[] {
    return this._roleInfo.getComments(player);
  }

  getTargets(turn: 'day' | 'night') {
    return this[`targets_${turn}`];
  }

  getTargetIcon(turn: 'day' | 'night') {
    if (!this._roleInfo) return '';
    return this._roleInfo[`target_${turn}`](this.state);
  }

  toggleTarget(turn: 'day' | 'night', target: string) {
    const targets = this[`targets_${turn}`];
    if (targets[this.id] && targets[this.id] == target) delete targets[this.id];
    else targets[this.id] = target;
  }

  isTurnReady(turn: 'day' | 'night'): boolean {
    if (!this._roleInfo) return false;
    return this._roleInfo[`ready_${turn}`](this.state);
  }
}

export class RolesService extends Service {
  #list: Role[] = [];

  async add(item: Role): Promise<Role> {
    return new Promise((resolve) => {
      item.updatedAt = new Date();
      this.#list.push(item);
      resolve(item);
    })
  }

  async get(id: number): Promise<Role> {
    return new Promise((resolve, reject) => {
      const found = this.#list[id];
      if (found) {
        resolve(found);
      }
      else {
        reject({ status: 404, message: 'Not Found' });
      }
    })
  }

  async list(): Promise<Role[]> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(this.#list), 1500);
    });
  }
}

export const $Roles = new RolesService();

import { RouteRecordRaw, createRouter, createWebHashHistory } from "vue-router";
import { $auth } from "@/modules/auth";
import PhotoBoot from "@/views/PhotoBoot.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/photo-boot",
    name: "PhotoBoot",
    meta: {
      public: true,
      css: 'page-photo'
    },
    component: PhotoBoot
  },
  {
    path: "/",
    name: "Lobby",
    meta: {
      css: 'page-lobby'
    },
    component: () =>
      import(/* webpackChunkName: "lobby" */ "../views/Lobby.vue")
  },
  {
    path: "/game/:id",
    name: "Game",
    meta: {
      css: 'page-game'
    },
    component: () =>
      import(/* webpackChunkName: "game" */ "../views/Game.vue")
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.meta.public) return next();
  const user = $auth.currentUser.value;
  if (user && user.name && user.image) return next();
  next({ name: 'PhotoBoot' });
});

export default router;
